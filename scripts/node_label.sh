#!/bin/bash

docker node ls 

echo "Enter the node's hostname below:"

read hostname

echo "Enter the node label tag below:"

read label 

docker node update --label-add node=$label $hostname

docker node promote $hostname

echo "The label on node $hostname is setup as node=$label"
