#!/bin/bash

echo "Installiing docker..."

sudo apt-get update

sudo apt-get upgrade -y

sudo apt install apt-transport-https ca-certificates curl software-properties-common -y

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"

sudo apt-get update

sudo apt-get install docker-ce  git -y

sudo usermod -aG docker $USER

sudo mkdir -p /opt/postgres

sudo chown $USER:$USER /opt/postgres

#curl -fsSL https://raw.githubusercontent.com/CWSpear/local-persist/master/scripts/install.sh | sudo bash

sudo systemctl restart docker

echo "Installing related plugin..."

curl -fsSL https://raw.githubusercontent.com/CWSpear/local-persist/master/scripts/install.sh | sudo bash

sudo systemctl restart docker

sudo docker pull pavouk0/postgres-xl:XL_10_R1_1-313-g31dfe47