#!/bin/bash -u

stack=$1

nodes=(db_coord_1 db_coord_2 db_data_1 db_data_2)
#-------------------------------------------------------------------------------
function log() {
    local msg="$*"

    echo "$(date -u '+%Y-%m-%dT%H:%M:%S%z'), $msg"
}

function startup() {
    log "========================================"
}

function shutdown() {
    log "----------------------------------------"
}

function srv() {
    local node=$1
    shift

    docker exec "$node" "$@"
}

function cmd() {
    local node=$1
    shift

    log "$@"
    srv "$node" psql -c "$@"
}

function get_container() {
    local node=$1

    id=$(docker service ps -f 'desired-state=running' -q "${stack}_${node}" |
        head -n1)
    echo "${stack}_${node}.1.${id}"
}

trap shutdown EXIT
#-------------------------------------------------------------------------------
startup

for node in "${nodes[@]}"
do
    ctnr=$(get_container "$node")

    cmd "$ctnr" "CREATE NODE coord_1 WITH (TYPE = 'coordinator', HOST = 'db_coord_1', PORT = 5432);"
    cmd "$ctnr" "CREATE NODE coord_2 WITH (TYPE = 'coordinator', HOST = 'db_coord_2', PORT = 5432);"
    cmd "$ctnr" "CREATE NODE data_1  WITH (TYPE = 'datanode',    HOST = 'db_data_1',  PORT = 5432);"
    cmd "$ctnr" "CREATE NODE data_2  WITH (TYPE = 'datanode',    HOST = 'db_data_2',  PORT = 5432);"
#    cmd "$ctnr" "CREATE NODE data_3  WITH (TYPE = 'datanode',    HOST = 'db_data_3',  PORT = 5432);"
    cmd "$ctnr" "ALTER  NODE coord_1 WITH (TYPE = 'coordinator', HOST = 'db_coord_1', PORT = 5432);"
    cmd "$ctnr" "ALTER  NODE coord_2 WITH (TYPE = 'coordinator', HOST = 'db_coord_2', PORT = 5432);"
    cmd "$ctnr" "ALTER  NODE data_1  WITH (TYPE = 'datanode',    HOST = 'db_data_1',  PORT = 5432);"
    cmd "$ctnr" "ALTER  NODE data_2  WITH (TYPE = 'datanode',    HOST = 'db_data_2',  PORT = 5432);"
#    cmd "$ctnr" "ALTER  NODE data_3  WITH (TYPE = 'datanode',    HOST = 'db_data_3',  PORT = 5432);"
    cmd "$ctnr" "SELECT pgxc_pool_reload();"
    cmd "$ctnr" "SELECT * FROM pgxc_node;"
done

sleep 10
#################################################################################################################################
#Generating gtm node necessary files

echo "======="
echo "gtm node initialization..."
echo "======="

docker ps | grep $stack | grep gtm | awk '{ print $1 }'

for i in $( docker ps | grep $stack | grep gtm | awk '{ print $1 }' ); do docker exec $i pgxc_ctl; done

VOLUME=/opt/postgres

sudo cp ../pgxc_ctl.conf $VOLUME/db_gtm_1/pgxc_ctl/

sleep 10
#################################################################################################################################
#Changing password for 'postgres' user

echo "======="
echo "Enter the Password for user postgres:"
echo "======="
read PASS

for i in $(docker ps | grep $stack | grep db_coord_1 | awk '{ print $1 }'); do  docker exec -it $i psql -c "ALTER USER postgres WITH PASSWORD '$PASS';"; done




sudo find $VOLUME -type f -name "pg_hba.conf" -exec sudo sed -i '84s/trust/password/' {} +


sleep 10
#################################################################################################################################

#Restarting Stack to reflact changes...

echo "================="
echo "Restarting stack to reflact changes. This might take some time. Please seat back and relax for sometime..."
echo "================="

docker ps | grep $stack |  awk '{ print $1 }' > ~/containers.txt

for i in $( cat ~/containers.txt ); do docker container stop $i; done

rm -rf ~/containers.txt


#################################################################################################################################
